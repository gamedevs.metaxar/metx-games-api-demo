﻿using System;
namespace GameBackend.Models
{
    public class GameBackendDatabaseSettings
    {
        public string ConnectionString { get; set; } = null!;

        public string DatabaseName { get; set; } = null!;

    }
}

