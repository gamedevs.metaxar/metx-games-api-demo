﻿using GameBackend.Models;
using GameBackend.Services;
using GameBackend.Middleware;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Net.Http;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.Security.Cryptography;

var builder = WebApplication.CreateBuilder(args);

builder.Configuration.AddJsonFile("appsettings.json");

builder.Services.Configure<GameBackendDatabaseSettings>(
    builder.Configuration.GetSection("GameBackendDatabaseSettings"));

builder.Services.AddScoped<IUserService, UserService>();
builder.Services.AddScoped<IScoreService, ScoreService>();
builder.Services.AddScoped<ITokenRequestService, TokenRequestService>();
builder.Services.AddScoped<ITokenClaimService, TokenClaimService>();
builder.Services.AddScoped<IAutosignerTokenService, AutosignerTokenService>();

// Add HttpClient and IHttpClientFactory
builder.Services.AddHttpClient();

// Generate a random secret key with 128 bits (16 bytes) length for JWT authentication
byte[] secretKeyBytes = new byte[16];
using (var rng = RandomNumberGenerator.Create())
{
    rng.GetBytes(secretKeyBytes);
}
string secretKey = Convert.ToBase64String(secretKeyBytes);

var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secretKey));

builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer(options =>
    {
        options.TokenValidationParameters = new TokenValidationParameters
        {
            ValidateIssuerSigningKey = true,
            ValidateAudience = false,
            ValidateIssuer = false,
            IssuerSigningKey = key, // The key used for signature validation (Secret key)
        };
    });

// Add services to the container.
builder.Services.AddControllers().AddJsonOptions(options =>
{
    options.JsonSerializerOptions.PropertyNamingPolicy = null;
});

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseRouting();

// Add CORS policy
app.UseCors(policy => policy
    .AllowAnyOrigin()
    .AllowAnyMethod()
    .AllowAnyHeader());

// Add JWT authentication
app.UseAuthentication();
app.UseAuthorization();

app.UseHttpsRedirection(); // Move this after authentication setup

// Add exception handling middleware
app.UseExceptionHandler("/error"); // Replace "/error" with your error handling endpoint

app.UseEndpoints(endpoints =>
{
    endpoints.MapControllers();
});

app.Run();
