using GameBackend.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GameBackend.Services
{
    public interface IGameBackendService
    {
        Task<List<Game>> GetAsync(string collectionName);
        Task<Game?> GetAsync(string collectionName, string id);
        Task CreateAsync(string collectionName, Game newGame);
        Task UpdateAsync(string collectionName, string id, Game updatedGame);
        Task RemoveAsync(string collectionName, string id);
    }
}
