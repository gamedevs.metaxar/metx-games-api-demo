using GameBackend.Models;
using System.Threading.Tasks;

namespace GameBackend.Services
{
    public interface ITokenRequestService
    {
        Task<TokensReqUpdateResult> UpdateTokenReq(string collectionName, string userId, string? hash = null);
    }
}
