using GameBackend.Models;
using GameBackend.Middleware;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System.Threading.Tasks;
using System;


namespace GameBackend.Services
{
    public class UserService : IUserService
    {
        private readonly IMongoDatabase _database;

        public UserService(IOptions<GameBackendDatabaseSettings> gameBackendDatabaseSettings)
        {
            var mongoClient = new MongoClient(gameBackendDatabaseSettings.Value.ConnectionString);
            _database = mongoClient.GetDatabase(gameBackendDatabaseSettings.Value.DatabaseName);
        }

        // Method to get the collection by name
        private IMongoCollection<Player> GetCollectionByName(string collectionName)
        {
            return _database.GetCollection<Player>(collectionName);
        }

        public async Task<Player?> GetUser(string collectionName, string userId)
        {
            var collection = GetCollectionByName(collectionName);
            return await collection.Find(x => x.UserId == userId).FirstOrDefaultAsync();
        }

        public async Task<string> CreateNewUser(string collectionName, string userId, User newUser)
        {

            // Create a new Player object and set its properties
            var player = new Player
            {
                UserId = userId,
                UserData = new User
                {
                    GameID = collectionName,
                    TokenID = newUser.TokenID,
                    LoginTime = DateTime.UtcNow
                },
                ScoreData = new Score
                {
                    DailyScore = 0,
                    TotalScore = 0,
                    DSUpdated = DateTime.UtcNow,
                    TSUpdated = DateTime.UtcNow
                },
                TokensReqData = new TokensReq
                {
                    TokensReqValue = 0,
                    Txnhash = "empty",
                    TRUpdated = DateTime.UtcNow
                },
                TokensClaimData = new TokensClaim
                {
                    TokensClaimed = 0,
                    TCUpdated = DateTime.UtcNow
                }
            };

            var collection = GetCollectionByName(collectionName);
            await collection.InsertOneAsync(player);

            // Generate a JWT token using the separate utility function
            var secretKey = "xar-K1yKcEICAWuh10ZylQOs-bearer-3AQ3fOPMueqBlJS7f7UH-admin-P1ESJbighL6htWzLvPwX";
            var tokenExpiryInMinutes = 1440;

            var jwtToken = Authentication.GenerateJwtToken(userId, secretKey, tokenExpiryInMinutes);

            return jwtToken;
        }

        public async Task<string> LoginUser(string collectionName, string userId, User updatedUser)
        {
            var collection = GetCollectionByName(collectionName);

            var filter = Builders<Player>.Filter.Eq(x => x.UserId, userId);

            var update = Builders<Player>.Update
                .Set(x => x.UserData.LoginTime, DateTime.UtcNow);

            await collection.UpdateOneAsync(filter, update);

            // Generate a JWT token using the separate utility function
            var secretKey = "xar-K1yKcEICAWuh10ZylQOs-bearer-3AQ3fOPMueqBlJS7f7UH-admin-P1ESJbighL6htWzLvPwX";
            var tokenExpiryInMinutes = 1440;

            var jwtToken = Authentication.GenerateJwtToken(userId, secretKey, tokenExpiryInMinutes);

            return jwtToken;
        }
    }
}
