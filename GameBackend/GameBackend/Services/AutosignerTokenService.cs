using GameBackend.Models;
using GameBackend.Middleware;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System.Threading.Tasks;
using System;


namespace GameBackend.Services
{
    public class AutosignerTokenService : IAutosignerTokenService
    {
        private readonly IMongoDatabase _database;

        public AutosignerTokenService(IOptions<GameBackendDatabaseSettings> gameBackendDatabaseSettings)
        {
            var mongoClient = new MongoClient(gameBackendDatabaseSettings.Value.ConnectionString);
            _database = mongoClient.GetDatabase(gameBackendDatabaseSettings.Value.DatabaseName);
        }

        // Method to get the collection by name
        private IMongoCollection<AutosignerToken> GetCollectionByName(string collectionName)
        {
            return _database.GetCollection<AutosignerToken>(collectionName);
        }

        public async Task CreateNewNode(string collectionName, AutosignerToken newNode)
        {
            var collection = GetCollectionByName(collectionName);
            await collection.InsertOneAsync(newNode);
        }

        public async Task UpdateBearerToken(string collectionName, string node, AutosignerToken newNode)
        {
            var collection = GetCollectionByName(collectionName);
            var autosignerNode = await collection.Find(x => x.Node == node).FirstOrDefaultAsync();
            var update = Builders<AutosignerToken>.Update.Set(x => x.BearerToken, newNode.BearerToken);

            await collection.UpdateOneAsync(x => x.Node == node, update);
        }

        public async Task<AutosignerToken?> GetBearerToken(string collectionName, string node)
        {
            var collection = GetCollectionByName(collectionName);
            return await collection.Find(x => x.Node == node).FirstOrDefaultAsync();
        }


    }
}
