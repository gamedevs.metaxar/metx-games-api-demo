# Store Score and Tokens in Database

## MongoDB Database

Database: MetXGames

Collections:

1. Game Id / Game Name - store games data
2. ~ metx-secure - store bearer tokens for autosigner
3. History Claim - store history of players after successful claim the tokens

## EndPoints

### User

1. POST New User
   - create new user document (first time play the game)
   - post all int data as 0
   - update time
   - generate jwt token
2. PUT Login
   - update user login time
   - generate jwt token
3. GET User Game Data
   - retrieve user data from gameid collection

### Score

1. PUT Daily score
   - current score as query parameter
   - limit score as query parameter
   - current score must not exceed limit score

### Tokens Request

1. PUT Tokens Request
   - txn hash as query parameter
   - score from total score field in player documents, will update to token request field
   - score from total score field will reset
   - hash will be updated in txn hash field

### Tokens Claim

1. GET Check Claim Status
   - autosigner url as query parameter
   - get bearer token from `~ metx-secure` collection and set as bearer header for autosigner url, run to check if the txn succeed.
   - if txn succeed true,
   - - the amount from tokens request will update to token claim field, and post new document to `History Claim` collection.
   - - the amount from tokens request will reset
   - if txn succeed fail,
   - - the amount from tokens request will update to total score field
   - - the amount from tokens request will reset

### AUtosigner Bearer

1. POST New Node & Bearer

   - body: node and bearer token
   - create new document for new node and store bearer token

2. PUT Update Bearer

   - node in url
   - body: bearer token

3. GET Bearer Token
   - node in url
