const admin = require("firebase-admin");
require('dotenv').config();

const getAll = async (req, res) => {
    try {
        const snapshot = await admin.database().ref().once("value");
        const data = snapshot.val();
        return res.status(200).send(data);
      } catch (error) {
        console.error(error);
        return res.status(500).send(error);
      }
};

module.exports = {
  getAll,
};
