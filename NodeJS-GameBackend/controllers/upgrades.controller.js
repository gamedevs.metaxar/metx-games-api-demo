const admin = require("firebase-admin");
// const jwt = require('jsonwebtoken');
require('dotenv').config();
const moment = require('moment-timezone');
const timezone = 'Asia/Singapore';
// const axios = require('axios');

const upgrades = async (req, res) => {
  try {
    const { gameID, user } = req.params;
    let newUpgrades = parseInt(req.query.upgrade);

    if (!gameID || !user || isNaN(newUpgrades)) {
      return res.status(400).json({
        success: false,
        message: 'Game ID, user, and score are required and must be numbers.'
      });
    }

    const now = moment().tz(timezone);
    const time = now.format('YYYY-MM-DD HH:mm:ss'); 
    const ref = admin.database().ref(`${gameID}/${user}/e_Upgrade`);

    // Retrieve the current daily score and total score from the database
    const scoreSnapshot = await ref.once('value');
    let dailyUpgrade = scoreSnapshot.child('a_DailyUpgrade').val() || 0;
    let monthlyUpgrade = scoreSnapshot.child('c_MonthlyUpgrade').val() || 0;
    let totalUpgrade = scoreSnapshot.child('e_TotalUpgrade').val() || 0;

    // Add new score to the daily score and total score
    dailyUpgrade += newUpgrades;
    monthlyUpgrade += newUpgrades
    totalUpgrade += newUpgrades;

    await ref.update({ a_DailyUpgrade: dailyUpgrade, b_DU_Updated: time, c_MonthlyUpgrade: monthlyUpgrade, d_MU_Updated: time, e_TotalUpgrade: totalUpgrade, f_TU_Updated: time });

    const viewModel = {
      success: true,
      message: "Daily and total scores updated",
      data: {
        dailyUpgrade,
        monthlyUpgrade,
        totalUpgrade,
        time
      },
    };

    return res.status(200).json(viewModel);
  } catch (error) {
    console.error(error);
    return res.status(500).json({
      success: false,
      message: "Internal server error"
    });
  }
};

module.exports = {
  upgrades,
};
