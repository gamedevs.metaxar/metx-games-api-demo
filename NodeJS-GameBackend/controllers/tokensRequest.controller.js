const admin = require("firebase-admin");
const jwt = require('jsonwebtoken');
require('dotenv').config();
const moment = require('moment-timezone');
const timezone = 'Asia/Singapore';
const axios = require('axios');

const tokensRequest = async (req, res) => {
  try {
    const { gameID, user } = req.params;
    const { eventID } = req.query;
    const { hash } = req.query;
    const now = moment().tz(timezone);
    const time = now.format('YYYY-MM-DD HH:mm:ss');

    let ref;
    if (eventID !== undefined && eventID !== null && eventID !== "") {
      ref = admin.database().ref(`${gameID}/${user}/zEvent_${eventID}`);
    } else {
      ref = admin.database().ref(`${gameID}/${user}`);
    }
    const tokensReqRef = ref.child('c_TokensReq');
    const scoreRef = ref.child('b_Score');

    // Retrieve the current total score from the database
    const scoreSnapshot = await scoreRef.once('value');
    let totalScore = scoreSnapshot.child('d_TotalScore').val();

    // Ensure totalScore is a number, if not default it to 0
    if (typeof totalScore !== 'number') {
      totalScore = 0;
    }

    const amount = totalScore; // set amount equal to the total score

    // Ensure there's a request for tokens and a hash provided
    if (!amount || !hash) {
      return res.status(200).json({
        success: false,
        message: "There is no token request made or no hash provided.",
        data: {
          eventID,
          totalScore: amount,
          hash
        },
      });
    }

    const tokensReqSnapshot = await tokensReqRef.once('value');
    const existingHash = tokensReqSnapshot.val() ? tokensReqSnapshot.val().b_TxnHash : null;

    // If there's already a hash, then we can't proceed with another request until it's processed
    if (existingHash) {
      return res.status(200).json({
        success: false,
        message: "A token request is already in process. Please wait until it's finished.",
        data: {
          eventID,
          hash
        },
      });
    }

    await Promise.all([
      // Update the tokens request data
      tokensReqRef.update({ a_TokensReq: amount, b_TxnHash: hash, c_TR_Updated: time }),
      // Reset the total score to 0
      scoreRef.update({ d_TotalScore: 0, e_TS_Updated: time })
    ]);

    const viewModel = {
      success: true,
      message: "Tokens Requested updated and total score reset",
      data: {
        eventID,
        amount,
        hash,
        time,
      },
    };
    return res.status(200).json(viewModel);

  } catch (error) {
    console.error(error);
    return res.status(500).send(error);
  }
};

const highScoreTokensRequest = async (req, res) => {
  try {
    const { gameID, user } = req.params;
    const { eventID } = req.query;
    const { hash } = req.query;
    const now = moment().tz(timezone);
    const time = now.format('YYYY-MM-DD HH:mm:ss');

    let ref;
    if (eventID !== undefined && eventID !== null && eventID !== "") {
      ref = admin.database().ref(`${gameID}/${user}/zEvent_${eventID}`);
    } else {
      ref = admin.database().ref(`${gameID}/${user}`);
    }
    const tokensReqRef = ref.child('c_TokensReq');
    const highScoreRef = ref.child('g_HighScore');
    const tokensClaimRef = ref.child('d_TokensClaim');

    const tokensClaimSnapshot = await tokensClaimRef.once('value');
    const amountTokensClaim = tokensClaimSnapshot.child('g_TokensClaimed').val();

    // Retrieve the current total score from the database
    const highScoreSnapshot = await highScoreRef.once('value');
    let highScore;
    if (eventID !== undefined && eventID !== null && eventID !== "") {
      highScore = highScoreSnapshot.child(eventID).val()
    } else {
      highScore = highScoreSnapshot.child('HighScore').val();
    }

    const amount = highScore - amountTokensClaim; // set amount equal to the total score

    // Ensure there's a request for tokens and a hash provided
    if (!amount) {
      return res.status(400).json({
        success: false,
        message: "There is no high score token request made.",
        data: {
          eventID,
          amount,
          hash
        },
      });
    }

    if (amount < 0) {
      return res.status(400).json({
        success: false,
        message: "High score below than expected (Amount cannot lower than 0)",
        data: {
          eventID,
          amount,
          hash
        },
      });
    }

    const tokensReqSnapshot = await tokensReqRef.once('value');
    const existingHash = tokensReqSnapshot.val() ? tokensReqSnapshot.val().b_TxnHash : null;

    // If there's already a hash, then we can't proceed with another request until it's processed
    if (existingHash) {
      return res.status(400).json({
        success: false,
        message: "A token request is already in process. Please wait until it's finished.",
        data: {
          eventID,
          hash
        },
      });
    }

    await Promise.all([
      // Update the tokens request data
      tokensReqRef.update({ a_TokensReq: amount, b_TxnHash: hash, c_TR_Updated: time }),
    ]);

    const viewModel = {
      success: true,
      message: "Tokens Requested updated",
      data: {
        eventID,
        amount,
        hash,
        time,
      },
    };
    return res.status(200).json(viewModel);

  } catch (error) {
    console.error(error);
    return res.status(500).send(error.message);
  }
};

module.exports = {
  tokensRequest,
  highScoreTokensRequest,
};
